import os
import sys
import hashlib
import sqlite3
import collections

from sqlite3 import DatabaseError

from . import _hashes
from . import _languages


def _xor(x, y):
    return bytes(a ^ b for a, b in zip(x, y))


def _dist(h1, h2):
    return sum(sum((x >> i) & 0x01 for i in range(8)) for x in _xor(h1, h2))


DBMatch = collections.namedtuple(
    "DBMatch", ("dist", "weight", "line_begin", "line_end", "author", "fid")
)

CodeSegment = collections.namedtuple(
    "CodeSegment", ("file", "author", "weight", "line_begin", "line_end")
)
Match = collections.namedtuple("Match", ("ref", "src", "dist"))

File = collections.namedtuple("File", ("name", "content", "lexer"))


def _get_language_from_name(lang):
    try:
        return next(l for l in _languages.languages if lang == l.name)
    except StopIteration:
        return None


class StorageConfig:
    def __init__(self, nblocks=32, min_weight=3):
        """
        Configuration for storage

        Parameters
        ----------
        nblocks : int, optional
          The number of blocks for partioning.  All similarity search must be
          made for a distance less than `nblocks`.  Increasing the number of
          blocks allows similarity search with a higher distance, but increase
          the database size and the consumed time.
          Default: 32.
        min_weight : int, optional
          The minimal weight of stored tree. When a search is made, a filter
          with a minimal weight of tree is applied, this minimal weight must be
          greater or equal than this parameter. Increasing this value decrease
          the database size and decrease the consumed time.
          Default: 5.
        """
        if nblocks not in (1, 2, 4, 8, 16, 32, 64):
            raise ValueError
        self._nblocks = nblocks
        self._min_weight = min_weight

    @property
    def nblocks(self):
        return self._nblocks

    @property
    def min_weight(self):
        return self._min_weight

    @property
    def block_size_bytes(self):
        return 512 // self._nblocks // 8

    def __repr__(self):
        return f"StorageConfig(nblocks={self._nblocks}, min_weight={self._min_weight})"


DBLang = collections.namedtuple("DBLang", ("lang", "conf"))


class AnalyzeConfig:
    def __init__(self, max_dist=24, min_weight=10):
        """
        Configuration for analyze

        Parameters
        ----------
        max_dist : int, optional
          The maximum allowed distance. Increasing this value increase the
          tolerance, but increase the number of false positive.
          Default: 24
        min_weight : int, optional
          Only search for subtree with weight greater or equal `min_weight`.
          If not provided, search in all the database. Must be greater or equal
          than the min_weight of the database. Increasing this value increase
          the minimal size of code to considers matches.
          Default: 10
        """
        self._max_dist = max_dist
        self._min_weight = min_weight

    @property
    def max_dist(self):
        return self._max_dist

    @property
    def min_weight(self):
        return self._min_weight

    def __repr__(self):
        return (
            f"AnalyzeConfig(max_dist={self._max_dist}, min_weight={self._min_weight})"
        )


class MatchInCommonKnowledge(Exception):
    pass


class DBVersionError(Exception):
    pass


class FileDuplicateError(Exception):
    pass


class LanguageUnknownError(Exception):
    pass


_DATABASE_VERSION = 3


class DB:
    @classmethod
    def languages_list(cls):
        "Return the supported languages"
        return [l.name for l in _languages.languages]

    def __init__(
        self, filename=None, *, create=False, languages=None, conf=None, lang_conf=None
    ):
        """DB instance for storing indexed AST hashes

        Parameters
        ----------
        filename : str, optional
          The filename for persistant store with sqlite3. If not provided,
          in-memory store is used, without persistency. If a filename is
          provided, but the file does not exist, the db is created, if the file
          the connexion is made to existing db.
        create : bool, optional
          If `create` is False, the db is not created is the file doesn't
          already exists, and a FileNotFoundError is raised. This parameter have
          no effect for a non persistent storage.
        languages : list, optional
          List of language names. To have the list of the supported language,
          have a look to DB.languages_list().
          Default: all supported languages are activated.
        conf : StorageConfig, optional
          The global storage configuration. This parameter have a effect only on
          database creation, and have no effect with a existing database.
          Default: See StorageConfig()
        lang_conf : dict of {str: StorageConfig}, optional
          The configuration for each language. The key must be language name,
          and value the StorageConfig object. This parameter is overseed the
          global config for given languages. This parameter have a effect only
          on database creation, and have no effect with a existing database.
        """

        if filename is None:
            filename = ":memory:"
        else:
            if not create:
                if not os.path.isfile(filename):
                    raise FileNotFoundError
            else:
                if os.path.isfile(filename):
                    os.remove(filename)
        self._conn = sqlite3.connect(filename)

        self._cur = self._conn.cursor()
        self._cur.execute("pragma synchronous = 0;")

        try:
            self._cur.execute(
                """
                SELECT
                    name
                FROM 
                    sqlite_master 
                WHERE 
                    type='table' AND name='params';
                """
            )
            self._cur.fetchone()
        except DatabaseError as e:
            raise e

        if create:
            if languages is None:
                languages = [l.name for l in _languages.languages]
            if conf is None:
                conf = StorageConfig()
            if lang_conf is None:
                lang_conf = {}
            notfoundlang = [
                lang for lang in languages if _get_language_from_name(lang) is None
            ]
            if not isinstance(conf, StorageConfig):
                raise TypeError("conf is not a StorageConfig instance")
            if notfoundlang:
                raise ValueError(f"Following languages are not found: {notfoundlang}")
            notfoundkey = [lang for lang in lang_conf if lang not in languages]
            if notfoundkey:
                raise ValueError(
                    f"Following languages in conf are not activated: {notfoundkey}"
                )
            for k, v in lang_conf.items():
                if not isinstance(v, StorageConfig):
                    raise TypeError(f"lang_conf[{k!r}] is not a StorageConfig instance")
            self._create_tables(languages, conf, lang_conf)

        params = {
            name: value
            for name, value in self._cur.execute("SELECT name, value FROM params;")
        }
        if (
            "database_version" not in params
            or params["database_version"] != _DATABASE_VERSION
        ):
            raise DBVersionError

        langs = set(k.split(":")[0] for k in params.keys() if ":" in k)
        self._languages = tuple(
            DBLang(
                _get_language_from_name(lang),
                StorageConfig(
                    nblocks=params[lang + ":nblocks"],
                    min_weight=params[lang + ":min_weight"],
                ),
            )
            for lang in langs
        )

    @property
    def languages(self):
        return self._languages

    def _create_tables(self, languages, conf, lang_conf):
        self._cur.execute(
            """
            CREATE TABLE params (
                name TEXT PRIMARY KEY,
                value INTEGER
                );
            """
        )
        self._cur.execute(
            "INSERT INTO params (name, value) VALUES (?,?);",
            ("database_version", _DATABASE_VERSION),
        )

        self._cur.execute(
            """
            CREATE TABLE files (
                fid INTEGER PRIMARY KEY,
                author TEXT,
                path TEXT,
                content BLOB
                );
            """
        )

        self._cur.execute(
            """
            CREATE TABLE files_hash (
                hash BLOB,
                author TEXT,
                PRIMARY KEY (hash, author)
                );
            """
        )

        for lang in languages:
            if lang in lang_conf:
                lc = lang_conf[lang]
            else:
                lc = conf
            self._cur.execute(
                "INSERT INTO params (name, value) VALUES (?,?);",
                (f"{lang}:min_weight", lc.min_weight),
            )
            self._cur.execute(
                "INSERT INTO params (name, value) VALUES (?,?);",
                (f"{lang}:nblocks", lc.nblocks),
            )

            for block in range(lc.nblocks):
                self._cur.execute(
                    f"""
                    CREATE TABLE hash_{lang}_{block:02} (
                        block_key BLOB,
                        hash BLOB,
                        weight INTEGER,
                        line_begin INTEGER,
                        line_end INTEGER,
                        author TEXT,
                        fid INTEGER REFERENCES files,
                        PRIMARY KEY (block_key, hash, weight, line_begin, line_end, author, fid)
                        );
                    """
                )
        self._conn.commit()

    def _find_file_lang(self, filename):
        try:
            return next(
                dbl
                for dbl in self._languages
                if any(filename.endswith(ext) for ext in dbl.lang.file_extensions)
            )
        except StopIteration:
            return None

    def is_recognized(self, filename):
        if self._find_file_lang(filename) is not None:
            return True
        return False

    def add_file_in_store(
        self, filename: str, author: str, on_error="print", on_dup="print"
    ):
        """Index and add a file in the store.

        Parameters
        ----------
        filename : str
          The file to add.
        author : str
          The author of the file. Use 'common' to add a file in common
          knowledge.
        on_error : str or callable,
          The behavior to have for a fail in AST construction. If "raise", a
          exception is raised, if "print" a warning is printed on stderr and the
          execution continue, if a callable, the error message is passed the the
          callable and the execution continue.
        on_dup : str or callable,
          The behavior to have for a duplicate file with same author. If
          "raise", a exception is raised, if "print" a warning is printed on
          stderr and the execution continue, if a callable, the error message is
          passed the the callable and the execution continue.
        """

        dbl = self._find_file_lang(filename)
        if dbl is None:
            if hasattr(on_error, "__call__"):
                on_dup(f"language of file {filename!r} is not recognized")
            elif on_dup == "print":
                print(
                    f"language of file {filename!r} is not recognized", file=sys.stderr
                )
            else:
                raise FileDuplicateError
            return

        with open(filename, "r") as f:
            bcontent = f.read()

        file_hash = hashlib.sha512(bcontent.encode()).digest()
        self._cur.execute(
            "SELECT author FROM files_hash WHERE hash=? AND author=?;",
            (file_hash, author),
        )
        if self._cur.fetchone() is not None:
            if hasattr(on_dup, "__call__"):
                on_dup(f"file {filename!r} already exists for the author {author!r}")
            elif on_dup == "print":
                print(
                    f"file {filename!r} already exists for the author {author!r}",
                    file=sys.stderr,
                )
            else:
                raise FileDuplicateError
            return

        try:
            tree = dbl.lang.get_tree_hashes(bcontent)
        except _hashes.ASTError as e:
            if hasattr(on_error, "__call__"):
                on_error(
                    f"file {filename!r} have Invalid syntax. This file is not added in the database"
                )
                return
            elif on_error == "print":
                print(f"invalid syntax: {filename}", file=sys.stderr)
                return
            else:
                raise e

        hashes = _hashes.flat_hashes(tree)

        self._cur.execute(
            "INSERT INTO files_hash (hash, author) VALUES (?,?);", (file_hash, author)
        )
        self._cur.execute(
            "INSERT INTO files (author, path, content) VALUES (?,?,?);",
            (author, filename, bcontent),
        )
        fid = self._cur.lastrowid
        for csh in hashes:
            if csh.weight >= dbl.conf.min_weight and csh.linerange is not None:
                h = csh.hash
                for block in range(dbl.conf.nblocks):
                    self._cur.execute(
                        f"""
                        INSERT OR REPLACE INTO hash_{dbl.lang.name}_{block:02}
                        (
                            block_key,
                            hash,
                            weight,
                            line_begin,
                            line_end,
                            author,
                            fid
                        )
                        VALUES
                        (?,?,?,?,?,?,?);
                        """,
                        (
                            h[
                                dbl.conf.block_size_bytes
                                * block : dbl.conf.block_size_bytes
                                * (block + 1)
                            ],
                            h,
                            csh.weight,
                            csh.linerange[0],
                            csh.linerange[1],
                            author,
                            fid,
                        ),
                    )
        self._conn.commit()

    def add_dir_in_store(
        self, dirname: str, author: str, on_error="print", on_dup="print"
    ):
        """Index and add all recognized files of a directory in the store.

        Parameters
        ----------
        dirname : str
          The directory which contains files to add.
        author : str
          The author of the file. Use 'common' to add a file in common
          knowledge.
        on_error : str or callable,
          The behavior to have for a fail in AST construction. If "raise", a
          exception is raised, if "print" a warning is printed on stderr and the
          execution continue, if a callable, the error message is passed the the
          callable and the execution continue.
        on_dup : str or callable,
          The behavior to have for a duplicate file with same author. If
          "raise", a exception is raised, if "print" a warning is printed on
          stderr and the execution continue, if a callable, the error message is
          passed the the callable and the execution continue.
        """
        while dirname.endswith(os.sep):
            dirname = dirname[: -len(os.sep)]
        for dirpath, _, filenames in os.walk(dirname, followlinks=True):
            for filename in filenames:
                dbl = self._find_file_lang(filename)
                if dbl is not None:
                    self.add_file_in_store(
                        dirpath + os.sep + filename,
                        author=author,
                        on_error=on_error,
                        on_dup=on_dup,
                    )

    def _find_match(
        self, h: bytes, author: str, dbl: DBLang, min_weight: int, max_dist: int
    ):
        matches = set()
        for block in range(dbl.conf.nblocks):
            matches.update(
                m
                for m in (
                    DBMatch(_dist(h, row[0]), row[1], row[2], row[3], row[4], row[5])
                    for row in self._cur.execute(
                        f"""SELECT
                            hash, weight, line_begin, line_end, author, fid
                        FROM
                            hash_{dbl.lang.name}_{block:02}
                        WHERE
                            block_key=? AND author!=? AND weight>=?
                    """,
                        (
                            h[
                                dbl.conf.block_size_bytes
                                * block : dbl.conf.block_size_bytes
                                * (block + 1)
                            ],
                            author,
                            min_weight,
                        ),
                    )
                )
                if m.dist <= max_dist
            )
        if "common" in set(m.author for m in matches):
            raise MatchInCommonKnowledge
        if not matches:
            return None
        best_match = min(
            matches, key=lambda m: (m.dist, -m.weight, -(m.line_end - m.line_begin))
        )
        return best_match

    def _validate_config(self, conf, lang_conf):
        if lang_conf is None:
            lang_conf = {}
        if conf is not None and not isinstance(conf, AnalyzeConfig):
            raise TypeError("config must be a AnalyzeConfig instance")
        langs = set(dbl.lang.name for dbl in self._languages)
        notfoundkeys = [l for l in lang_conf.keys() if l not in langs]
        if notfoundkeys:
            raise ValueError(f"following langs are not activated: {notfoundkeys}")
        for l, c in lang_conf.items():
            if not isinstance(c, AnalyzeConfig):
                raise ValueError(f"lang_conf[{l!r}] must be a AnalyzeConfig instance")

        complete_lang_conf = {}
        for l in langs:
            dbl = next(dbl for dbl in self._languages if dbl.lang.name == l)
            if l in lang_conf:
                complete_lang_conf[l] = lang_conf[l]
            elif conf is not None:
                complete_lang_conf[l] = conf
            else:
                complete_lang_conf[l] = AnalyzeConfig(
                    max_dist=dbl.conf.nblocks * 3 // 4,
                    min_weight=dbl.conf.min_weight * 2,
                )
            if complete_lang_conf[l].min_weight < dbl.conf.min_weight:
                raise ValueError(
                    f"for lang {l}, min_weight for analyze can not be lesser than min_weight in database"
                )
            if complete_lang_conf[l].max_dist >= dbl.conf.nblocks:
                raise ValueError(
                    f"for lang {l}, max_dist for analyze can not be greater or equal than nblocks in database"
                )
        return complete_lang_conf

    def analyze_file(
        self, filename: str, author: str, conf=None, lang_conf=None, on_error="print"
    ):
        """Find approximate matches in the database.

        Parameters
        ----------
        filename : str
          The file to analyze.
        author : str
          The author to exclude. All matches with this author are not reported.
        conf : AnalyzeConfig, optional
          The global configuration for analysis. See AnalyzeConfig().
        lang_conf : dict of {str: AnalyzeConfig}, optional
          The specific configuration for each language. Each config overseed
          global configuration.
        on_error : str or callable,
          The behavior to have for a fail in AST construction. If "raise", a
          exception is raised, if "print" a warning is printed on stderr and the
          execution continue, if a callable, the error message is passed the the
          callable and the execution continue.

        Returns
        -------
        list of matches :
          describe the list of matches.
        reffile :
          the file (name, content and lexer).
        dict of src files :
          the dict of files (names and contents) referenced in list of matches.
        """

        complete_lang_conf = self._validate_config(conf, lang_conf)

        dbl = self._find_file_lang(filename)
        if dbl is None:
            if hasattr(on_error, "__call__"):
                on_error(f"language of file {filename!r} is not recognized.")
                return
            elif on_error == "print":
                print(
                    f"language of file {filename!r} is not recognized.", file=sys.stderr
                )
                return
            else:
                raise LanguageUnknownError

        with open(filename) as f:
            bcontent = f.read()
        tree = dbl.lang.get_tree_hashes(bcontent)
        matches = self._recursively_find_matches(
            tree, filename, author, dbl, complete_lang_conf[dbl.lang.name]
        )
        return (
            matches,
            File(filename, bcontent, dbl.lang.get_pygment_lexer),
            {
                fid: self._get_file(fid, dbl.lang.get_pygment_lexer)
                for fid in set(m.src.file for m in matches)
            },
        )

    def _recursively_find_matches(self, tree, filename: str, author: str, dbl, conf):
        current_hash, children_hashes = tree
        if current_hash.weight < conf.min_weight:
            return []
        try:
            match = self._find_match(
                current_hash.hash,
                author,
                dbl=dbl,
                min_weight=conf.min_weight,
                max_dist=conf.max_dist,
            )
        except MatchInCommonKnowledge:
            return []
        if match is not None:
            return [
                Match(
                    CodeSegment(
                        filename,
                        author,
                        current_hash.weight,
                        current_hash.linerange[0],
                        current_hash.linerange[1],
                    ),
                    CodeSegment(
                        match.fid,
                        match.author,
                        match.weight,
                        match.line_begin,
                        match.line_end,
                    ),
                    match.dist,
                )
            ]
        ret = []
        for subtree in children_hashes:
            ret.extend(
                self._recursively_find_matches(
                    subtree, filename, author, dbl=dbl, conf=conf
                )
            )
        return ret

    def _get_file(self, fid: int, lexer):
        self._cur.execute("SELECT path, content FROM files WHERE fid=?", (fid,))
        name, content = self._cur.fetchone()
        return File(name, content, lexer)
