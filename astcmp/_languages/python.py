import ast
from pygments.lexers import PythonLexer

from .._ast_common import ASTChild, ASTNode, ASTError
from .._hashes import Language


class Python(Language):
    Name = "Python"
    name = "python"
    names = ("python",)

    file_extensions = (".py",)

    @classmethod
    def _get_ast_from_tree(cls, t):
        children = []
        for field, c in ast.iter_fields(t):
            if isinstance(c, list):
                for subchild in c:
                    if isinstance(subchild, ast.AST):
                        ast_subchild = cls._get_ast_from_tree(subchild)
                        children.append(ASTChild(field, ast_subchild))
            elif isinstance(c, ast.AST):
                ast_child = cls._get_ast_from_tree(c)
                children.append(ASTChild(field, ast_child))
        lineno = t.lineno if hasattr(t, "lineno") else None
        return ASTNode(type(t).__name__, lineno, children)

    @classmethod
    def get_ast(cls, code):
        try:
            tree = ast.parse(code)
        except:
            raise ASTError
        return cls._get_ast_from_tree(tree)

    @classmethod
    def get_pygment_lexer(cls):
        return PythonLexer()
