import collections

ASTNode = collections.namedtuple("ASTNode", ("name", "lineno", "children"))
ASTChild = collections.namedtuple("ASTChild", ("field", "node"))


class ASTError(Exception):
    pass
