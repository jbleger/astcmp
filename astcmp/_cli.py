import os
import sys
import argparse
import textwrap

from ._db import DB, StorageConfig, AnalyzeConfig, DatabaseError, DBVersionError
from ._export_html import analysis_result_to_html, summary_to_html
from ._hashes import ASTError
from ._languages import languages as LANGUAGES

LANGUAGE_NAMES = set(l.name for l in LANGUAGES)


def _lang_nblocks(value):
    if ":" in value:
        svalue = value.split(":")
        if len(svalue) != 2:
            raise TypeError
        lang, val = svalue
        if not lang in LANGUAGE_NAMES:
            raise ValueError
    else:
        lang = None
        val = value
    ival = int(val)
    if not ival in (1, 2, 4, 8, 16, 32, 64):
        raise ValueError
    return (lang, ival)


def _lang_min_weight_max_dist(value):
    if ":" in value:
        svalue = value.split(":")
        if len(svalue) != 2:
            raise TypeError
        lang, val = svalue
        if not lang in LANGUAGE_NAMES:
            raise ValueError
    else:
        lang = None
        val = value
    ival = int(val)
    return (lang, ival)


def _html_writable_file(filename):
    if not filename.endswith(".html"):
        raise TypeError
    return argparse.FileType("w")(filename)


def parse_args():
    parser = argparse.ArgumentParser(
        description=textwrap.dedent(
            """\
            A tool to detect plagiarism using hash-based similarity between Abstract Syntax Trees.
            """
        ),
        epilog="",
    )

    subparsers = parser.add_subparsers(dest="subcommand", title="subcommands")

    parser_create = subparsers.add_parser(
        "create",
        help="Create empty database for indexing files.",
        description="Create empty database for indexing files.",
    )

    parser_add = subparsers.add_parser(
        "add",
        help="Add files or directories in the database for a given author",
        description="Add files or directories in the database for a given author",
    )

    parser_analyze = subparsers.add_parser(
        "analyze",
        help="Analyze files or directories for a given author",
        description="Analyze files or directories for a given author.",
    )

    parser_autoadd = subparsers.add_parser(
        "auto-add",
        help="Add all files from directories in the database, deducing authors name from directory names.",
        description="Add all files from directories in the database, deducing authors names from directories names.",
    )

    parser_autoanalyze = subparsers.add_parser(
        "auto-analyze",
        help="Analyze all files from directories, deducing authors names from directories names.",
        description="Analyze all files from directories, deducing authors names from directories names.",
    )

    for p in (
        parser_create,
        parser_add,
        parser_analyze,
        parser_autoadd,
        parser_autoanalyze,
    ):
        p.add_argument(
            "-q",
            "--quiet",
            dest="quiet",
            action="count",
            default=0,
            help=textwrap.dedent(
                """\
                Quiet mode. Display only errors and warnings on stderr. Can be
                repeated two times to display only errors, and three times to
                display nothing.
                """
            ),
        )
        p.add_argument(
            "-d",
            "--dbfile",
            dest="dbfile",
            required=True,
            type=str,
            metavar="DATABASE_FILE",
            help="Database file.",
        )

    parser_create.add_argument(
        "-l",
        "--language",
        dest="languages",
        type=str,
        action="append",
        metavar="LANG",
        choices=LANGUAGE_NAMES,
        default=[],
        help=textwrap.dedent(
            f"""\
            Create a database supporting the language LANG. This option can be
            specified multiple time to support multiple languages. If no
            languages are provided, all the languages are supported.
            Supported languages are: {', '.join(LANGUAGE_NAMES)}.
            """
        ),
    )

    parser_create.add_argument(
        "-N",
        "--nblocks",
        dest="nblocks",
        type=_lang_nblocks,
        default=[],
        action="append",
        help=textwrap.dedent(
            """\
            The number of blocks for partioning. This parameter have a effect
            only on database creation, e.g. "-N 32".  All similarity search must
            be made for a distance less than `nblocks`.  Increasing the number
            of blocks allows similarity search with a higher distance, but
            increase the database size and the consumed time for adding and
            analysis.  The value must be a power of 2 lesser or equal than 64.
            Default: 32.
            Language specific value can be given with specifing the language,
            e.g. "-N python:16". This option can me repeated for different
            languages. All language specific values override the global value.
            """
        ),
    )

    parser_create.add_argument(
        "-W",
        "--store-min-weight",
        dest="min_weight",
        type=_lang_min_weight_max_dist,
        action="append",
        default=[],
        help=textwrap.dedent(
            """\
            The minimal weight of stored tree. This parameter have a effect only
            on database creation, e.g. "-W 10".  When a search is made, a filter
            with a minimal weight of tree is applied, this minimal weight must
            be greater or equal than this parameter. Increasing this value
            decrease the database size and decrease the consumed time for adding
            and analysis.
            Default: 10.
            Language specific value can be given with specifing the language,
            e.g. "-W python:12". This option can me repeated for different
            languages. All language specific values override the global value.
            """
        ),
    )

    for p in (parser_add, parser_analyze):
        p.add_argument(
            "-A",
            "--author",
            dest="author",
            required=True,
            help=(
                textwrap.dedent(
                    """\
                    String to identify the author of the provided code to forbid a
                    author matching to themselves.
                    """
                )
                + textwrap.dedent(
                    """\
                    Use the author "common" to add code in the common knowledge
                    to prevent false positive matching to this code.
                    """
                    if p is parser_add
                    else ""
                )
            ),
        )

    for p in (parser_analyze, parser_autoanalyze):
        p.add_argument(
            "-n",
            "--nbits",
            default=[],
            type=_lang_min_weight_max_dist,
            action="append",
            dest="max_dist",
            help=textwrap.dedent(
                """\
                Maximal number of different bit between used hash and searched
                hashes in the database, e.g. "-n 24". Increasing the value
                increase the tolerance for matching, but increase the number of
                false positive. This value must be lesser than the number of
                blocks used in database creation.
                Default: 24 (if allowed by the database).
                Language specific value can be given with specifing the
                language, e.g. "-n python:15". This option can me repeated for
                different languages. All language specific values override the
                global value.
                """
            ),
        )
        p.add_argument(
            "-w",
            "--min-weight",
            default=[],
            type=_lang_min_weight_max_dist,
            action="append",
            dest="min_weight",
            help=textwrap.dedent(
                """\
                Minimal weight of subtree of AST used in similarity search, e.g.
                "-w 20". This weight is a image of the size of the code implied.
                Descreasing this value allow matches for small part of code, but
                increase the number of false positive.
                Default 20 (if allowed by the database).
                Language specific value can be given with specifing the
                language, e.g. "-w python:18". This option can me repeated for
                different languages. All language specific values override the
                global value.
                """
            ),
        )
        p.add_argument(
            "-o",
            "--output",
            type=_html_writable_file,
            dest="output",
            required=False if p is parser_analyze else True,
            help=(
                "File to write the html report."
                + (
                    " "
                    + textwrap.dedent(
                        """\
                        For unique file input, by default, the same basename
                        than the source file with the html extention is used.
                        For multiple file input and for directory input, this
                        argument is required.
                        """
                    )
                    if p is parser_analyze
                    else ""
                )
            ),
        )

    for p in (parser_add, parser_analyze):
        p.add_argument(
            dest="files",
            metavar="FILES_OR_DIRS",
            nargs="+",
            type=str,
            help=textwrap.dedent(
                f"""\
                File or directory to {'add' if p is parser_add else 'analyze'}.
                You can provide as many as you want.
                """
            ),
        )

    for p in (parser_autoadd, parser_autoanalyze):
        p.add_argument(
            dest="dirs",
            metavar="DIR",
            nargs="+",
            type=str,
            help=textwrap.dedent(
                f"""
                Directory to {'add' if p is parser_autoadd else 'analyze'}. For
                each directory, the author name used is tne directory name.
                """
            ),
        )

    args = parser.parse_args()
    if args.subcommand is None:
        parser.print_usage()
        sys.exit(1)
    return args


def _pathfromto(f, t):
    df = os.path.abspath(os.path.dirname(f))
    dt = os.path.abspath(os.path.dirname(t))
    bt = os.path.basename(t)
    c = os.path.commonprefix((df, dt))
    return os.path.relpath(c, df) + os.path.sep + os.path.relpath(dt, c) + os.sep + bt


def _verb(args, level, msg):
    if args.quiet <= level:
        print(["[info] ", "[WARNING] ", "[ERROR] "][level] + msg, file=sys.stderr)


def _access_db(args):
    try:
        db = DB(args.dbfile)
    except (DatabaseError, FileNotFoundError):
        _verb(args, 2, "Database file is invalid")
        sys.exit(1)
    except DBVersionError:
        _verb(args, 2, "Database file is build with a incompatabile version of astcmp")
        sys.exit(1)
    return db


def _get_lang_values(languages, default, l):
    try:
        fixed = next(val for lang, val in l if lang is None)
        default_value = lambda _: fixed
    except StopIteration:
        default_value = default
    preret = {lang: val for lang, val in l if lang is not None}
    ret = {}
    for lang in languages:
        if lang in preret:
            ret[lang] = preret[lang]
        else:
            ret[lang] = default_value(lang)
    return ret


def _analyze_args_lang_conf(args, db):
    languages = [dbl.lang.name for dbl in db.languages]
    default_min_weight = {
        dbl.lang.name: max(dbl.conf.min_weight, 20) for dbl in db.languages
    }
    default_max_dist = {
        dbl.lang.name: min(dbl.conf.nblocks - 1, 24) for dbl in db.languages
    }
    lang_min_weight = _get_lang_values(
        languages, lambda lang: default_min_weight[lang], args.min_weight
    )
    lang_max_dist = _get_lang_values(
        languages, lambda lang: default_max_dist[lang], args.max_dist
    )
    lang_conf = {
        lang: AnalyzeConfig(
            min_weight=lang_min_weight[lang], max_dist=lang_max_dist[lang]
        )
        for lang in languages
    }

    error = False
    for dbl in db.languages:
        if lang_conf[dbl.lang.name].min_weight < dbl.conf.min_weight:
            _verb(
                args,
                2,
                f"For language {dbl.lang.name}"
                f" `min_weight={lang_conf[dbl.lang.name].min_weight}` (-W)"
                f" can not be lesser than database `min_weight={dbl.conf.min_weight}` (-W)",
            )
            error = True
        if lang_conf[dbl.lang.name].max_dist >= dbl.conf.nblocks:
            _verb(
                args,
                2,
                f"For language {dbl.lang.name}"
                f" `max_dist={lang_conf[dbl.lang.name].max_dist}` (-n)"
                f" can not be lesser than database `nblocks={dbl.conf.nblocks}` (-N)",
            )
            error = True
    if error:
        sys.exit(1)
    return lang_conf


def main_create(args):
    if args.languages:
        languages = args.languages
    else:
        languages = LANGUAGE_NAMES
    lang_min_weight = _get_lang_values(languages, lambda _: 20, args.min_weight)
    lang_nblocks = _get_lang_values(languages, lambda _: 32, args.nblocks)
    lang_conf = {
        lang: StorageConfig(
            min_weight=lang_min_weight[lang], nblocks=lang_nblocks[lang]
        )
        for lang in languages
    }

    DB(args.dbfile, create=True, languages=languages, lang_conf=lang_conf)
    return None


def main_add(args):
    db = _access_db(args)
    for filename in args.files:
        if os.path.isfile(filename):
            _verb(args, 0, f"Adding file {filename!r} with author {args.author!r}")
            db.add_file_in_store(
                filename,
                args.author,
                on_error=lambda x: _verb(args, 1, x),
                on_dup=lambda x: _verb(args, 1, x),
            )
        elif os.path.isdir(filename):
            _verb(args, 0, f"Adding directory {filename!r} with author {args.author!r}")
            db.add_dir_in_store(
                filename,
                args.author,
                on_error=lambda x: _verb(args, 1, x),
                on_dup=lambda x: _verb(args, 1, x),
            )
        else:
            _verb(args, 1, f"Not found file or dir: {filename!r}")


def main_analyze(args):
    db = _access_db(args)
    lang_conf = _analyze_args_lang_conf(args, db)

    files_to_analyze = []
    for filename in args.files:
        if os.path.isfile(filename) and db.is_recognized(filename):
            files_to_analyze.append(filename)
        elif os.path.isdir(filename):
            for d, _, fs in os.walk(filename):
                files_to_analyze.extend(
                    d + os.sep + f for f in fs if db.is_recognized(f)
                )
        else:
            _verb(args, 1, f"Not found file or dir: {filename!r}")

    if not files_to_analyze:
        return
    htmlmainfile = None
    if len(files_to_analyze) == 1:
        if args.output is not None:
            output = lambda _: args.output
        else:
            output = lambda x: open(x + ".html", "w")
    else:
        if args.output is None:
            print(
                "Argument -o/--output is required for multiple file input or directory input",
                file=sys.stderr,
            )
            sys.exit(1)
        htmlmainfile = args.output.name
        outputdir = os.path.splitext(args.output.name)[0] + "_astcmp_files"
        if not os.path.isdir(outputdir):
            try:
                os.mkdir(outputdir)
            except:
                _verb(args, 2, f"Directory {outputdir!r} can not be created.")
                sys.exit(1)

        def output(x):
            relative = os.path.dirname(x)
            if relative:
                subpath = outputdir + "/" + relative
                try:
                    os.makedirs(subpath, exist_ok=True)
                except:
                    _verb(args, 2, f"Directory {subpath!r} can not be created.")
                    sys.exit(1)
            return open(os.path.normpath(outputdir + "/" + x + ".html"), "w")

    summary = {}
    for filename in files_to_analyze:
        _verb(args, 0, f"Analyzing file {filename!r} with author {args.author!r}")
        syntaxerror = False
        try:
            matches, reffile, files = db.analyze_file(
                filename, args.author, lang_conf=lang_conf
            )
        except ASTError:
            syntaxerror = True
            _verb(
                args,
                1,
                f"file {filename!r} have invalid syntax. This file is not analyzed.",
            )
        fo = output(filename)
        analysis_result_to_html(matches, reffile, files, fo)
        fo.close()
        nblines = reffile.content.count("\n")
        if not syntaxerror:
            score = sum(m.src.line_end - m.src.line_begin + 1 for m in matches) / (
                nblines if nblines > 0 else 1
            )
        else:
            score = 0
        if htmlmainfile is not None:
            summary[filename] = (_pathfromto(htmlmainfile, fo.name), score, syntaxerror)
    if htmlmainfile is not None:
        _verb(args, 0, f"Write summary in output {htmlmainfile!r}")
        summary_to_html(
            {args.author: files_to_analyze}, summary, lang_conf, args.output
        )


def main_autoadd(args):
    db = _access_db(args)
    for dirname in args.dirs:
        if os.path.isdir(dirname):
            _verb(args, 0, f"Adding directory {dirname!r} with author {dirname!r}")
            db.add_dir_in_store(
                dirname,
                dirname,
                on_error=lambda x: _verb(args, 1, x),
                on_dup=lambda x: _verb(args, 1, x),
            )
        else:
            _verb(args, 1, f"Not found directory: {dirname!r}")


def main_autoanalyze(args):
    db = _access_db(args)
    lang_conf = _analyze_args_lang_conf(args, db)

    files_to_analyze = {}
    for dirname in args.dirs:
        if os.path.isdir(dirname):
            files_to_analyze[dirname] = []
            for d, _, fs in os.walk(dirname):
                files_to_analyze[dirname].extend(
                    d + os.sep + f for f in fs if db.is_recognized(f)
                )
            files_to_analyze[dirname].sort()
        else:
            _verb(args, 1, f"Not found directory: {dirname!r}")

    htmlmainfile = args.output.name
    outputdir = os.path.splitext(args.output.name)[0] + "_astcmp_files"
    if not os.path.isdir(outputdir):
        try:
            os.mkdir(outputdir)
        except:
            _verb(args, 2, f"Directory {outputdir!r} can not be created.")
            sys.exit(1)

    def output(x):
        relative = os.path.dirname(x)
        if relative:
            subpath = outputdir + "/" + relative
            try:
                os.makedirs(subpath, exist_ok=True)
            except:
                _verb(args, 2, f"Directory {subpath!r} can not be created.")
                sys.exit(1)
        return open(os.path.normpath(outputdir + "/" + x + ".html"), "w")

    summary = {}
    for author, filenames in files_to_analyze.items():
        for filename in filenames:
            _verb(args, 0, f"Analyzing file {filename!r} with author {author!r}")
            syntaxerror = False
            try:
                matches, reffile, files = db.analyze_file(
                    filename, author, lang_conf=lang_conf
                )
            except ASTError:
                _verb(
                    args,
                    1,
                    f"file {filename!r} have invalid syntax. This file is not analyzed.",
                )
                syntaxerror = True
            fo = output(filename)
            analysis_result_to_html(matches, reffile, files, fo)
            fo.close()
            nblines = reffile.content.count("\n")
            if not syntaxerror:
                score = sum(m.src.line_end - m.src.line_begin + 1 for m in matches) / (
                    nblines if nblines > 0 else 1
                )
            else:
                score = 0
            summary[filename] = (_pathfromto(htmlmainfile, fo.name), score, syntaxerror)

    _verb(args, 0, f"Write summary in output {htmlmainfile!r}")
    summary_to_html(files_to_analyze, summary, lang_conf, args.output)


def main():
    args = parse_args()

    if args.subcommand == "create":
        main_create(args)
    if args.subcommand == "add":
        main_add(args)
    if args.subcommand == "analyze":
        main_analyze(args)
    if args.subcommand == "auto-add":
        main_autoadd(args)
    if args.subcommand == "auto-analyze":
        main_autoanalyze(args)


if __name__ == "__main__":
    main()
