import ast
import hashlib
import operator
import functools
from typing import List

import numpy as np

from ._ast_common import ASTNode, ASTChild, ASTError


def _xor(x, y):
    return bytes(a ^ b for a, b in zip(x, y))


def _circular_perm(x):
    return np.concatenate((x[1:], x[0:1]))


def _to_np(w):
    return (
        np.array(np.unpackbits(np.array([x for x in w], dtype=np.uint8)), dtype=int) * 2
        - 1
    )


def _from_np(w):
    return bytes(np.packbits(w > 0))


def _hashfun(x):
    return hashlib.sha512(x.encode("utf8")).digest()


class CodeSegmentHash:
    def __init__(
        self, *, name=None, linerange=None, lineno=None, weight=1, hashcount=None
    ):
        assert (name is None and hashcount is not None) or (
            name is not None and hashcount is None
        )
        assert not ((lineno is not None) and (linerange is not None))
        if hashcount is not None:
            self._hashcount = hashcount
        else:
            self._hashcount = _to_np(_hashfun(name))
        if lineno is not None:
            self._linerange = (lineno, lineno)
        else:
            self._linerange = linerange
        self._weight = weight

    @property
    def linerange(self):
        return self._linerange

    @property
    def hash(self):
        return _from_np(self._hashcount)

    @property
    def weight(self):
        return self._weight

    def subnode(self, field):
        h = _to_np(_hashfun(field))
        return CodeSegmentHash(
            hashcount=self._hashcount * h,
            linerange=self._linerange,
            weight=self._weight,
        )

    def update_name(self, name):
        self._hashcount *= _to_np(_hashfun(name))

    def update_lineno(self, lineno):
        if lineno is not None:
            if self._linerange is None:
                self._linerange = (lineno, lineno)
            else:
                self._linerange = (
                    min(lineno, self._linerange[0]),
                    max(lineno, self._linerange[1]),
                )

    def __add__(self, oth):
        linerange = None
        if self._linerange is not None:
            if oth._linerange is not None:
                linerange = (
                    min(self._linerange[0], oth._linerange[0]),
                    max(self._linerange[1], oth._linerange[1]),
                )
            else:
                linerange = self._linerange
        else:
            if oth._linerange is not None:
                linerange = oth._linerange
        return CodeSegmentHash(
            linerange=linerange,
            weight=self._weight + oth._weight,
            hashcount=self._hashcount + oth._hashcount,
        )

    def __repr__(self):
        hashhex = bytearray(self.hash).hex()
        return f"<CodeSegmentHash>(bytes.fromhex({hashhex!r}), linerange={self._linerange!r}, weight={self._weight!r})"


class Language:
    @classmethod
    def get_tree_hashes(cls, code):
        node = cls.get_ast(code)
        return cls._get_ast_from_node(node)

    @classmethod
    def _get_ast_from_node(cls, node: ASTNode):
        subtree_hashes = []
        children_hashes = []
        for child in node.children:
            h, sths = cls._get_ast_from_node(child.node)
            subtree_hashes.append((h, sths))
            children_hashes.append(h.subnode(child.field))
        if children_hashes:
            node_h = functools.reduce(operator.add, children_hashes)
            node_h.update_name(node.name)
            node_h.update_lineno(node.lineno)
        else:
            node_h = CodeSegmentHash(name=node.name, lineno=node.lineno)
        return (node_h, subtree_hashes)

    @classmethod
    def get_ast(cls, code) -> List[ASTNode]:
        return []


def flat_hashes(global_h_and_sthashes):
    global_h, sthashes = global_h_and_sthashes
    ret = [global_h]
    for sth in sthashes:
        ret.extend(flat_hashes(sth))
    return ret
