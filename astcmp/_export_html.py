import io
import textwrap

import pygments
from pygments import highlight
from pygments.formatters import HtmlFormatter


class MinorHtmlFormatter(HtmlFormatter):
    def __init__(self, line_begin, line_end):
        self.__line_begin = line_begin
        self.__line_end = line_end
        HtmlFormatter.__init__(self, linenos="inline")

    def wrap(self, source, _outputfile=None):
        return self._wrap_code(source)

    def _wrap_code(self, source):
        for lo, (i, t) in enumerate(source):
            lineno = lo + 1
            if lineno < self.__line_begin - 3:
                continue
            if lineno > self.__line_end + 3:
                continue
            if self.__line_begin <= lineno <= self.__line_end:
                yield i, '<div class="srcmatch">' + t + "</div>"
            else:
                yield i, '<div class="srcnomatch">' + t + "</div>"


def get_formatted_file_subset(fileobj, line_begin, line_end):
    lexer = fileobj.lexer()
    formatter = MinorHtmlFormatter(line_begin=line_begin, line_end=line_end)
    return highlight(fileobj.content, lexer, formatter)


class MajorHtmlFormatter(HtmlFormatter):
    def __init__(self, matches, files):
        self.__matches = matches
        self.__files = files
        HtmlFormatter.__init__(self, linenos="inline")

    def wrap(self, source, _outputfile=None):
        return self._wrap_code(source)

    def _wrap_code(self, source):
        yield 0, '<div class="highlight"><pre>'
        for lo, (i, t) in enumerate(source):
            lineno = lo + 1
            try:
                match_begin = next(
                    m for m in self.__matches if m.ref.line_begin == lineno
                )
            except StopIteration:
                match_begin = None
            try:
                match_end = next(m for m in self.__matches if m.ref.line_end == lineno)
            except StopIteration:
                match_end = None
            preline = ""
            postline = ""
            if match_begin:
                preline = """<div class="refmatch" onclick="(function(s){s.display=s.display==='block'?'none':'block'})(this.nextElementSibling.style)">"""
            if match_end:
                postline = "</div>"
                postline += '<div class="srcsubset">'
                postline += f"""<span class="matchinfos"> match infos:"""
                postline += f""" dist={match_end.dist}"""
                postline += f""", subtree_weight=({match_end.ref.weight},{match_end.src.weight})"""
                postline += f""", filename={self.__files[match_end.src.file].name!r}"""
                postline += f""", author={match_end.src.author!r}"""
                postline += "</span>\n"
                postline += get_formatted_file_subset(
                    self.__files[match_end.src.file],
                    match_end.src.line_begin,
                    match_end.src.line_end,
                )
                postline += "</div>"

            yield i, preline + t + postline
        yield 0, "</pre></div>"


def analysis_result_to_html(
    matches, reffile, files, output=None, *, header="<h2>{filename}</h2>"
):
    """Write a analysis result to html.

    Parameters
    ----------
    matches : list
      list of matches as provided by `DB.analyze_file()`
    reffile : File namedtuple
      the file used for the analysis, as provided by `DB.analyze_file()`
    files : dict {fid: File namedtuple}
      the dict of file used in matches, as provided by `DB.analyze_file()`
    output : file-like object, optional
      the object to write the output (as a open writable file). Must have a
      `write()` method.
      If not provided, the output is returned as a str.
    header : str, optional
      header to include in html. The string is formatted with the key
      `filename`.

    Returns
    -------
    None or str
      Depending of the output parameter.
    """

    matches = list(matches)
    matches.sort(key=lambda m: (m.ref.line_begin, m.ref.line_end))
    nmatches = []
    last_lineno = -1
    for m in matches:
        if m.ref.line_begin > last_lineno:
            nmatches.append(m)
            last_lineno = m.ref.line_end
    matches = nmatches
    formatter = MajorHtmlFormatter(matches=matches, files=files)

    ret_as_str = False
    if output is None:
        ret_as_str = True
        output = io.StringIO()

    header = header.format(filename=reffile.name)

    output.write(
        textwrap.dedent(
            f"""\
            <!doctype html public "-//w3c//dtd html 4.01//en"
               "http://www.w3.org/tr/html4/strict.dtd">

            <html>
            <head>
              <title>{reffile.name}</title>
              <meta http-equiv="content-type" content="text/html; charset=utf-8">
              <style type="text/css">
            """
        )
    )
    output.write("pre { line-height: 125%; }\n")
    output.write("span.lineno { background-color: #f0f0f0; padding: 0 5px 0 5px; }\n")
    output.write("div.refmatch { background-color: #ffff12; }\n")
    output.write("div.srcmatch { background-color: #ffaa91; }\n")
    output.write("div.srcnomatch { background-color: #fbe9e7; }\n")
    output.write(
        "div.srcsubset { display: none; border:1px solid black; margin:20px;}\n"
    )
    output.write(formatter.get_style_defs())
    output.write("  </style>\n")
    output.write("</head>\n")
    output.write("<body>\n")
    output.write(header)
    output.write("\n")

    output.write(highlight(reffile.content, reffile.lexer(), formatter))

    output.write("</body>\n")
    output.write("</html>\n")

    if ret_as_str:
        ret = output.getvalue()
        output.close()
        return ret
    return None


def summary_to_html(files, summary, lang_conf, output):
    output.write(
        textwrap.dedent(
            f"""\
            <!doctype html public "-//w3c//dtd html 4.01//en"
               "http://www.w3.org/tr/html4/strict.dtd">

            <html>
            <head>
              <title>ASTcmp analysis</title>
              <meta http-equiv="content-type" content="text/html; charset=utf-8">
              <style type="text/css">
            """
        )
    )
    output.write("pre { line-height: 125%; }\n")
    output.write("span.match3 { background-color: #ffff00; }\n")
    output.write("span.match2 { background-color: #ffff55; }\n")
    output.write("span.match1 { background-color: #ffffaa; }\n")
    output.write("span.syntaxerror { background-color: #ffaaaa; }\n")
    output.write("  </style>\n")
    output.write("</head>\n")
    output.write("<body>\n")
    output.write("<h2>ASTcmp analysis</h2>\n")
    output.write("<div><pre>Parameters:\n")
    for lang, conf in lang_conf.items():
        output.write(f"  - language: {lang}\n")
        output.write(f"    - min_weight = {conf.min_weight}\n")
        output.write(f"    - max_dist = {conf.max_dist}\n")
    for i in range(3):
        output.write("\n")
    output.write("Result:\n\n")
    for author, filenames in files.items():
        output.write(f"  - {author}\n")
        for filename in filenames:
            htmlfile, score, syntaxerror = summary[filename]
            pcscore = score * 100
            if syntaxerror:
                sc, formattedfilename = (
                    "[------]",
                    f'<span class="syntaxerror">{filename}</span>',
                )
            elif pcscore > 5:
                sc, formattedfilename = (
                    f"[{pcscore:5.1f}%]",
                    f'<span class="match3">{filename}</span>',
                )
            elif pcscore > 2:
                sc, formattedfilename = (
                    f"[{pcscore:5.1f}%]",
                    f'<span class="match2">{filename}</span>',
                )
            elif pcscore > 0:
                sc, formattedfilename = (
                    f"[{pcscore:5.1f}%]",
                    f'<span class="match1">{filename}</span>',
                )
            else:
                sc, formattedfilename = "[  0.0%]", filename
            fline = f'    - {sc} <a href="{htmlfile}">{formattedfilename}</a>\n'
            output.write(fline)
    output.write("</pre></div>\n")
    output.write("</body>\n")
    output.write("</html>\n")

    output.write("\n")
