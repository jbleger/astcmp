# A tool to detect plagiarism using hash-based similarity between Abstract Syntax Trees.

The main idea is to create a hash robust to transformation for each subtree of
AST, and index this hashes in a database allowing search for plagiarism for a
code.

Currently, only python 3 is supported.

## Installation

```
pip install astcmp --index-url https://gitlab.com/api/v4/projects/47754788/packages/pypi/simple
```

## CLI Usage

In classical usage, we have 3 steps.

 - Creation of a database.
 - Feeding the database with existant code and actual code.
 - Comparing actual code to the database.

### Creation of the database

```
astcmp create -d mybase.db
```

You can specify following args:
 - `-N` the number of blocks for indexing.
 - `-W` the minimal weight to store hash.

Default choices are the best option to consider in first try.

### Feeding the database

#### Automatical feeding

If you have on directory by author, and if the directory name identify the
author, (in the example all this directory are contained in `reports/`), you can simply do:

```
astcmp auto-add -d mybase.db reports/*
```

#### Manual feeding

To add files from a author named _John Doe_, you can do:

```
astcmp add -d mybase.db -A "John Doe" path/to/a_file.py another/path/to/another_file.py
```

you can add by this way files or directories.

#### Common knowledge

Sometimes, some piece of code can be viewed as common knowledge, and all
detection should avoid detect this part of code. To add code in common
knowledge, you must use the author `common`

```
astcmp add -d mybase.db -A common a_file_in_common_knowledge.py
```

With that, all subtrees matching subtrees in common knowledge are ignored.

### Analyze code

#### Automatical analyze

If you have on directory by author, and if the directory name identify the
author, (in the example all this directory are contained in `reports/`), you can simply do:

```
astcmp auto-analyze -d mybase.db -o report.html reports/*
```

All files are analyzed, considering the directory name is the author name, a
html report is generated for each file, and a global html report named
`report.html` is generating with links to all file report.

Open `report.html` in your browser.

#### Manual analyze

For a file or a dir or many of them of a particular author, run the command:

 - For a single file:
   ```
   astcmp analyze -d mybase.db -A "John Doe" path/to/a_file.py
   ```
 - For multiple files or directory, specifing an output is mandatory:
   ```
   astcmp analyze -d mybase.db -o output.html -A "John Doe" path/to
   ```

## Troubleshooting

 - Too small parts of code are detected, this is not usefull: *increase
   `min_weight` (`-w`) in analyze.*
 - Small parts of code are not detected: *decrease
   `min_weight` (`-w`) in analyze. You may need decrease `min_weight` (`-W`) in
   database creation.*
 - Similar parts of code are not detected: *increase `max_dist` (-n) in analyze.
   You may need increase `nblocks` (-N) in database creation.*
 - Very different parts of code are detected as similar: *decrease `max_dist`
   (-n) in analyze.*
 - The computation is too time consuming or the database is too big: *decrease
   `nblocks` and increase `min_weight` in database creation. When you have
   found appropriate value for analyze, choose the same value for `min_weight`
   in database creation, and for `nblocks` the fist power of two which is
   strctly superior than max_dist.*

## Python module

The module is easy-to-use in python, usefull object are:

 - `astcmp.DB` for `DB` connection and creation. This object offers methods to
   add files and to perform analyzes.
 - `astcmp.analysis_result_in_html()` to write the result of a analysis in a
   html file.
